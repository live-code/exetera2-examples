'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">exetera2-examples documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-3ab661e8e70231a16aa74b7d759dbc0d"' : 'data-target="#xs-components-links-module-AppModule-3ab661e8e70231a16aa74b7d759dbc0d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-3ab661e8e70231a16aa74b7d759dbc0d"' :
                                            'id="xs-components-links-module-AppModule-3ab661e8e70231a16aa74b7d759dbc0d"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CoreModule.html" data-type="entity-link">CoreModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CoreModule-e2a067f3a2bcbcc71b1d803b1547d315"' : 'data-target="#xs-components-links-module-CoreModule-e2a067f3a2bcbcc71b1d803b1547d315"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CoreModule-e2a067f3a2bcbcc71b1d803b1547d315"' :
                                            'id="xs-components-links-module-CoreModule-e2a067f3a2bcbcc71b1d803b1547d315"' }>
                                            <li class="link">
                                                <a href="components/NavbarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NavbarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-CoreModule-e2a067f3a2bcbcc71b1d803b1547d315"' : 'data-target="#xs-injectables-links-module-CoreModule-e2a067f3a2bcbcc71b1d803b1547d315"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-CoreModule-e2a067f3a2bcbcc71b1d803b1547d315"' :
                                        'id="xs-injectables-links-module-CoreModule-e2a067f3a2bcbcc71b1d803b1547d315"' }>
                                        <li class="link">
                                            <a href="injectables/ThemeService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ThemeService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/HomeModule.html" data-type="entity-link">HomeModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-HomeModule-4a229d1ae9656e6eed0f6c9a6b3fd3c5"' : 'data-target="#xs-components-links-module-HomeModule-4a229d1ae9656e6eed0f6c9a6b3fd3c5"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-HomeModule-4a229d1ae9656e6eed0f6c9a6b3fd3c5"' :
                                            'id="xs-components-links-module-HomeModule-4a229d1ae9656e6eed0f6c9a6b3fd3c5"' }>
                                            <li class="link">
                                                <a href="components/HomeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HomeComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoginModule.html" data-type="entity-link">LoginModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LoginModule-e1b9365406fcc13f566a6eeaab271007"' : 'data-target="#xs-components-links-module-LoginModule-e1b9365406fcc13f566a6eeaab271007"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LoginModule-e1b9365406fcc13f566a6eeaab271007"' :
                                            'id="xs-components-links-module-LoginModule-e1b9365406fcc13f566a6eeaab271007"' }>
                                            <li class="link">
                                                <a href="components/LoginComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LoginComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LostpassComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LostpassComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RegistrationComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RegistrationComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SigninComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SigninComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoginRoutingModule.html" data-type="entity-link">LoginRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PexelsModule.html" data-type="entity-link">PexelsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PexelsModule-2ee03596da613b5efaac990638b37979"' : 'data-target="#xs-components-links-module-PexelsModule-2ee03596da613b5efaac990638b37979"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PexelsModule-2ee03596da613b5efaac990638b37979"' :
                                            'id="xs-components-links-module-PexelsModule-2ee03596da613b5efaac990638b37979"' }>
                                            <li class="link">
                                                <a href="components/PexelsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PexelsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SettingsModule.html" data-type="entity-link">SettingsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SettingsModule-0b993a9576bdbfac01cbad6a00d81cb5"' : 'data-target="#xs-components-links-module-SettingsModule-0b993a9576bdbfac01cbad6a00d81cb5"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SettingsModule-0b993a9576bdbfac01cbad6a00d81cb5"' :
                                            'id="xs-components-links-module-SettingsModule-0b993a9576bdbfac01cbad6a00d81cb5"' }>
                                            <li class="link">
                                                <a href="components/SettingsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SettingsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link">SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SharedModule-55de11a85c8fc30acb7d14dabf1c8415"' : 'data-target="#xs-components-links-module-SharedModule-55de11a85c8fc30acb7d14dabf1c8415"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-55de11a85c8fc30acb7d14dabf1c8415"' :
                                            'id="xs-components-links-module-SharedModule-55de11a85c8fc30acb7d14dabf1c8415"' }>
                                            <li class="link">
                                                <a href="components/CardComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HelloComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HelloComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/StaticMapComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">StaticMapComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TabbarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TabbarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/TvmazeModule.html" data-type="entity-link">TvmazeModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-TvmazeModule-d54b1b1d0a3c0f11f5995f40de29ef6c"' : 'data-target="#xs-components-links-module-TvmazeModule-d54b1b1d0a3c0f11f5995f40de29ef6c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-TvmazeModule-d54b1b1d0a3c0f11f5995f40de29ef6c"' :
                                            'id="xs-components-links-module-TvmazeModule-d54b1b1d0a3c0f11f5995f40de29ef6c"' }>
                                            <li class="link">
                                                <a href="components/TvmazeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TvmazeComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/Uikit1Module.html" data-type="entity-link">Uikit1Module</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-Uikit1Module-aaef336842cf38586ed4525e8cca5528"' : 'data-target="#xs-components-links-module-Uikit1Module-aaef336842cf38586ed4525e8cca5528"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-Uikit1Module-aaef336842cf38586ed4525e8cca5528"' :
                                            'id="xs-components-links-module-Uikit1Module-aaef336842cf38586ed4525e8cca5528"' }>
                                            <li class="link">
                                                <a href="components/Uikit1Component.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">Uikit1Component</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UsersModule.html" data-type="entity-link">UsersModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UsersModule-e13ecd1ef543d8651308299dea6c88ad"' : 'data-target="#xs-components-links-module-UsersModule-e13ecd1ef543d8651308299dea6c88ad"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UsersModule-e13ecd1ef543d8651308299dea6c88ad"' :
                                            'id="xs-components-links-module-UsersModule-e13ecd1ef543d8651308299dea6c88ad"' }>
                                            <li class="link">
                                                <a href="components/HelpComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HelpComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/InputBarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">InputBarComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/InputIconComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">InputIconComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UsersComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UsersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UsersCrudComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UsersCrudComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-UsersModule-e13ecd1ef543d8651308299dea6c88ad"' : 'data-target="#xs-injectables-links-module-UsersModule-e13ecd1ef543d8651308299dea6c88ad"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-UsersModule-e13ecd1ef543d8651308299dea6c88ad"' :
                                        'id="xs-injectables-links-module-UsersModule-e13ecd1ef543d8651308299dea6c88ad"' }>
                                        <li class="link">
                                            <a href="injectables/UsersService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UsersService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AppPage.html" data-type="entity-link">AppPage</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/HomeService.html" data-type="entity-link">HomeService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/City.html" data-type="entity-link">City</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Country.html" data-type="entity-link">Country</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Country-1.html" data-type="entity-link">Country</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Externals.html" data-type="entity-link">Externals</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Image.html" data-type="entity-link">Image</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ImageResult.html" data-type="entity-link">ImageResult</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Links.html" data-type="entity-link">Links</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Network.html" data-type="entity-link">Network</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Photo.html" data-type="entity-link">Photo</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Post.html" data-type="entity-link">Post</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Previousepisode.html" data-type="entity-link">Previousepisode</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Rating.html" data-type="entity-link">Rating</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Schedule.html" data-type="entity-link">Schedule</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Self.html" data-type="entity-link">Self</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Series.html" data-type="entity-link">Series</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Show.html" data-type="entity-link">Show</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Src.html" data-type="entity-link">Src</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/User.html" data-type="entity-link">User</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/User-1.html" data-type="entity-link">User</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Video.html" data-type="entity-link">Video</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/VideoFile.html" data-type="entity-link">VideoFile</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/VideoPicture.html" data-type="entity-link">VideoPicture</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/VideoResult.html" data-type="entity-link">VideoResult</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/typealiases.html" data-type="entity-link">Type aliases</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});