import { NgModule } from '@angular/core';
import { NavbarComponent } from './components/navbar.component';
import { RouterModule } from '@angular/router';
import { ThemeService } from './services/theme.service';

@NgModule({
  declarations: [
    NavbarComponent,
  ],
  exports: [
    NavbarComponent
  ],
  imports: [
    RouterModule,
  ],
  providers: [
    ThemeService
  ]
})
export class CoreModule {

}
