import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../services/theme.service';
import { HomeService } from '../../features/home/services/home.service';

@Component({
  selector: 'app-navbar',
  template: `
    <div 
      style="padding: 20px; height: 70px; margin-bottom: 10px;"
      [style.background]="themeService.theme === 'dark' ? '#222' : '#666'"
    >
      
      {{homeService.posts?.length}}
      <button [routerLink]="'home'">home</button>
      <button routerLink="login">login</button>
      <button routerLink="pexels">pexels</button>
      <button routerLink="tvmaze">tv maze</button>
      <button routerLink="users">users</button>
      <button routerLink="uikit1">UI KIT 1</button>
      <button routerLink="settings">settings</button>
    </div>
  `,
})
export class NavbarComponent {
  constructor(
    public themeService: ThemeService,
    public homeService: HomeService
  ) {}

  update() {
    console.log('update')
  }
}
