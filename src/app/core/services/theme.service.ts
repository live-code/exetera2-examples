import { Injectable } from '@angular/core';

export type Theme = 'dark' | 'light';

@Injectable()
export class ThemeService {
  private themeProp: Theme = 'dark';

  set theme(value: Theme) {
    // ...
    this.themeProp = value;
  }

  get theme(): Theme {
    return this.themeProp;
  }
}

