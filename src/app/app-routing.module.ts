import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

const routes = [
    { path: 'tvmaze', loadChildren: () => import('./features/tvmaze/tvmaze.module').then(m => m.TvmazeModule)},
    { path: 'pexels', loadChildren: () => import('./features/pexels/pexels.module').then(m => m.PexelsModule)},
    { path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule)},
    { path: 'uikit1', loadChildren: () => import('./features/uikit1/uikit1.module').then(m => m.Uikit1Module)},
    { path: 'settings', loadChildren: () => import('./features/settings/settings.module').then(m => m.SettingsModule)},
    {
      path: 'users',
      loadChildren: () => import('./features/users/users.module')
        .then(m => m.UsersModule)
    },
    { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
    { path: '**', redirectTo: 'home'},
  ];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule,
  ]
})
export class AppRoutingModule {}
