import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HelloComponent } from './components/hello.component';
import { StaticMapComponent } from './components/static-map.component';
import { CardComponent } from './components/card.component';
import { TabbarComponent } from './components/tabbar.component';


const COMPONENTS = [
  // shared
  HelloComponent,
  StaticMapComponent,
  CardComponent,
  TabbarComponent
];

@NgModule({
  declarations: [
    ...COMPONENTS
  ],
  exports: [
    ...COMPONENTS
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
