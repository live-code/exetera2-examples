import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

/**
 * un pannello bellissimo...
 */
@Component({
  selector: 'app-card',
  template: `
    <div 
      class="card text-dark"
      [ngClass]="'mb-' + marginBottom">
      <div 
        class="card-header"
        [ngClass]="headerClass"
        (click)="isOpen = !isOpen"
      >
        {{title}}
        <i
          *ngIf="icon"
          class="pull-right"
          [ngClass]="icon"
          (click)="iconClickHandler($event)"></i>
      </div>
      
      <div class="card-body" *ngIf="isOpen">
        <ng-content select=".header"></ng-content>
      </div>
      
      <div class="card-footer">
        <ng-content select=".footer"></ng-content>
      </div>
    </div>
  `
})
export class CardComponent  {
  /**
   * il titolo del pannello
   */
  @Input() title: string;
  /**
   * Margine sottostante
   */
  @Input() marginBottom: 0 | 1 | 2 | 3 | 4 | 5 = 2;
  @Input() headerClass: string;
  @Input() icon;
  /**
   * Evento emesso al click dell'icona, solo se presente
   */
  @Output() iconClick: EventEmitter<void> = new EventEmitter<void>();
  isOpen = true;

  iconClickHandler(event: MouseEvent) {
    event.stopPropagation()
    this.iconClick.emit()
  }
}

