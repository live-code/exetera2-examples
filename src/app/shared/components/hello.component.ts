import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-hello',
  template: `
    <h1 [style.color]="color">
      hello {{name}}
    </h1>
  `,
  styles: [
  ]
})
export class HelloComponent {
  @Input() name = 'Mario Rossi';
  @Input() color = 'yellow';
}
