import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-static-map',
  template: `
    <ng-container  *ngIf="city">
      <img
        [src]="API + city + '&zoom=' + zoom + '&size=100x100&key=' + TOKEN" alt="">
    </ng-container>
  `,
})
export class StaticMapComponent {
  @Input() city: string;
  @Input() zoom = 6;

  API = 'https://maps.googleapis.com/maps/api/staticmap?center=';
  TOKEN = 'AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k';

}
