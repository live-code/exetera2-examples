import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-tabbar',
  template: `
    <ul class="nav nav-tabs">
      <li 
        class="nav-item" 
        *ngFor="let tab of data"
        (click)="tabClick.emit(tab)"
      >
        <a 
          class="nav-link"
          [ngClass]="{'active': tab.id === active?.id}"
        >
          {{tab[labelField]}}
          
          <i
            (click)="iconCLickHandler(tab, $event)"
            class="fa" [ngClass]="icon" *ngIf="icon"></i>
        </a>
      </li>
    </ul>
  `,

})
export class TabbarComponent<T> {
  @Input() data: T[];
  @Input() active: T;
  @Input() icon: string;
  @Input() labelField = 'label';
  @Output() tabClick: EventEmitter<T> = new EventEmitter<any>();
  @Output() iconClick: EventEmitter<T> = new EventEmitter<any>();

  iconCLickHandler(tab: T, event: MouseEvent) {
    event.stopPropagation();
    this.iconClick.emit(tab);
  }
}
