import { Injectable } from '@angular/core';
import { Post } from '../model/post';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class HomeService {
  posts: Post[];

  constructor(private http: HttpClient) { }

  getPosts() {
    this.http.get<Post[]>('https://jsonplaceholder.typicode.com/posts')
      .subscribe(res => this.posts = res);
  }

  deleteHandler(id: number) {
    this.http.delete('https://jsonplaceholder.typicode.com/posts/' + id)
      .subscribe(res => {
          const index = this.posts.findIndex(p => p.id === id);
          this.posts.splice(index, 1);
        });
  }
}
