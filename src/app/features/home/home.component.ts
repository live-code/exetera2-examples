import { Component } from '@angular/core';
import { HomeService } from './services/home.service';

@Component({
  selector: 'app-home',
  template: `
    <li 
      *ngFor="let post of homeService.posts" 
      class="list-group-item text-black-50">
      {{post.title}}
      <button (click)="homeService.deleteHandler(post.id)">delete</button>
    </li>
  `,
})
export class HomeComponent {
  constructor(public homeService: HomeService) {
    homeService.getPosts();
  }
}
