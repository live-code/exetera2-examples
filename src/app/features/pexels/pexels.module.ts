import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PexelsComponent } from './pexels.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    PexelsComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      { path: '', component: PexelsComponent }
    ])
  ]
})
export class PexelsModule { }
