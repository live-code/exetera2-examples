import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ImageResult, Photo } from './model/photo';
import { VideoFile, VideoResult } from './model/video';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-pexels',
  template: `
    <form #f="ngForm" (ngSubmit)="submit()">
      <input class="big-input"  #inputText type="text" ngModel name="text" required>
      <select class="big-input"  [(ngModel)]="type" name="type" required (change)="changeType()">
        <option value="">Select a type</option>
        <option value="image">Image</option>
        <option value="video">Video</option>
      </select>
      <button type="submit" [disabled]="f.invalid" hidden>GO</button>
      <button (click)="resetHandler()">reset</button>
    </form>

    <div style="overflow: auto; width: 100%">
      <div style="display: flex">

        <!--image list-->
        <ng-container *ngIf="type === 'image'">
          <div style="flex-grow: 0" *ngFor="let img of images?.photos">
            <img [src]="img.src.small" alt="" (click)="selectedPhoto = img">
          </div>
        </ng-container>

        <!--video list-->
        <ng-container *ngIf="type === 'video'">
          <div style="flex-grow: 0; " *ngFor="let video of videos?.videos">
            <img [src]="video.image" width="200" alt="" (click)="selectedVideo = video.video_files[0]">
          </div>
        </ng-container>
      </div>
    </div>

    <img *ngIf="selectedPhoto && type === 'image'" [src]="selectedPhoto.src.medium" width="100%">
    <video *ngIf="selectedVideo && type === 'video'" [src]="selectedVideo.link" controls width="100%"></video>

    <div *ngIf="images?.photos.length === 0">non ci sono immagini</div>
    <div *ngIf="videos?.videos.length === 0">non ci sono video</div>

  `,
})
export class PexelsComponent implements OnInit, AfterViewInit {
  @Input() pippo: string;
  @ViewChild('f') form: NgForm;
  @ViewChild('inputText', { static: true }) inputText: ElementRef<HTMLInputElement>

  type: 'video' | 'image' | '' = '';
  images: ImageResult;
  videos: VideoResult;
  selectedPhoto: Photo;
  selectedVideo: VideoFile;

  constructor(private http: HttpClient) {
    console.log('ctr', this.inputText)
  }

  ngOnInit() {
    console.log('onInit', this.inputText)
  }

  ngAfterViewInit() {
    console.log('afterview', this.inputText)
    this.inputText.nativeElement.focus()
  }

  submit() {
    // this.type = f.value.type;
    if (this.type === 'image') {
      this.searchImage(this.form.value.text)
    } else {
      this.searchVideo(this.form.value.text)
    }
  }

  searchImage(text: string) {
    this.http.get<ImageResult>( 'https://api.pexels.com/v1/search?per_page=10&query=' + text, {
      headers: {
        Authorization: '563492ad6f9170000100000189ac030285b04e35864a33b95c2838be'
      }
    })
      .subscribe(res => {
        this.images = res;
        this.selectedPhoto = res.photos[0];
      })
  }
  searchVideo(text: string) {
    this.http.get<VideoResult>( 'https://api.pexels.com/videos/search?per_page=10&query=' + text, {
      headers: {
        Authorization: '563492ad6f9170000100000189ac030285b04e35864a33b95c2838be'
      }
    })
      .subscribe(res => {
        this.videos = res;
        this.selectedVideo = res.videos[0] ? res.videos[0].video_files[0] : null
      });
  }

  changeType() {
    this.images = null;
    this.videos = null;
    this.submit();
  }

  resetHandler() {
    this.form.reset({ type: ''});
    // pulizia modello
  }
}
