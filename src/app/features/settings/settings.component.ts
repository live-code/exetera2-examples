import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';

@Component({
  selector: 'app-settings',
  template: `
    <p>
      settings works!
    </p>
  `,
  styles: [
  ]
})
export class SettingsComponent implements OnInit {
  constructor(private themeService: ThemeService) {
    console.log(themeService.theme)

    setTimeout(() => {
      this.themeService.theme = 'light';
    }, 2000)
  }

  ngOnInit(): void {
  }

}
