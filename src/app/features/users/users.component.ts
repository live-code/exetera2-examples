import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-users',
  template: `
    
    <button routerLink="./list">Goto users</button>
    <button routerLink="./help">Goto Help</button>
    
    <hr>
    
    <router-outlet></router-outlet>
    
  `,
  styles: [`
    
  `]
})
export class UsersComponent {

}


