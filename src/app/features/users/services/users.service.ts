import { Injectable } from '@angular/core';
import { User } from '../model/user';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';

@Injectable()
export class UsersService {
  users: User[];
  error: boolean;

  constructor(private http: HttpClient) {}

  getUsers() {
    this.http.get<User[]>('http://localhost:3000/users')
      .subscribe(res => this.users = res)
  }

  deleteUser(user: User) {
    this.error = false
    this.http.delete(`http://localhost:3000/users/${user.id}`)
      .subscribe(
        () => {
          const index = this.users.findIndex(u => u.id === user.id)
          this.users.splice(index, 1)
        },
        err => this.error = true
      )

  }

  saveHandler(user: Partial<User>) {
    this.error = false;
    // const user = f.value as User;
    this.http.post<User>('http://localhost:3000/users', user)
      .subscribe(res => {
        this.users.push(res);
      })
  }
}
