import { Component, Input } from '@angular/core';
import { NgModel } from '@angular/forms';

@Component({
  selector: 'app-input-icon',
  template: `
    <i
      class="fa"
      [ngClass]="{
        'fa-asterisk':  inputModelRef.errors?.required,
        'fa-sort-numeric-asc':  inputModelRef.errors?.minlength,
        'fa-check':  inputModelRef.valid
      }"
      [style.color]="inputModelRef.invalid ? 'red' : 'green'"
    ></i>
  `,
  styles: [
  ]
})
export class InputIconComponent {
  @Input() inputModelRef: NgModel;
}
