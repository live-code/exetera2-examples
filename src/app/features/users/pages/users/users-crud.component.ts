import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-users-crud',
  template: `
    <hr>
    <div class="alert alert-danger" *ngIf="usersService.error">Errore server</div>
    <pre>{{inputName.errors | json}}</pre>
    <div class="container mt-2">
      <form
        #f="ngForm"
        (submit)="usersService.saveHandler(f.value)"
      >
        <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text">
              <app-input-icon [inputModelRef]="inputName"></app-input-icon>
            </div>
          </div>
       
          <app-input-bar [inputError]="inputName.errors"></app-input-bar>

          <input
            class="form-control"
            [ngClass]="{
              'is-invalid': inputName.invalid && f.dirty, 
              'is-valid': inputName.valid
            }"
            placeholder="Name"
            type="text"
            name="name"
            [ngModel]
            required minlength="5" #inputName="ngModel">
        </div>


        <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text">
              <app-input-icon [inputModelRef]="inputCity"></app-input-icon>
            </div>
          </div>
          <app-input-bar [inputError]="inputCity.errors"></app-input-bar>

          <input
            class="form-control"
            placeholder="City"
            type="text"
            name="city"
            [ngModel]
            #inputCity="ngModel"
            [ngClass]="{
              'is-invalid': inputCity.invalid && f.dirty, 
              'is-valid': inputCity.valid
            }"
            required
            minlength="4"
          >
        </div>


        <select class="form-control" name="gender" [ngModel]>
          <option [ngValue]="null">Select Gender</option>
          <option value="M">male</option>
          <option value="F">female</option>
        </select>
        <button type="submit" [disabled]="f.invalid">save</button>
      </form>

      <div *ngIf="!usersService.users">loading...</div>
      <div *ngIf="!usersService.users?.length">There are no users</div>

      <li
        class="list-group-item text-dark"
        *ngFor="let user of usersService.users"
        [ngClass]="{
          'male': user.gender === 'M',
          'female': user.gender === 'F'
        }"
      >
        {{user.name}} <br>
        <i class="fa fa-trash pull-right" (click)="usersService.deleteUser(user)">delete</i>
        <div *ngIf="user.birthday">BirthDay: {{user.birthday | date}}</div>

        <img
          *ngIf="user.city"
          [src]="'https://maps.googleapis.com/maps/api/staticmap?center=' + user.city + '&zoom=5&size=200x100&key=AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k'"
          alt=""
        >
      </li>
    </div>
  `,
  styles: [`
    
  `]
})
export class UsersCrudComponent {

  constructor(public usersService: UsersService) {
    usersService.getUsers();
  }
}


