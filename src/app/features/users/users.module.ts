import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputBarComponent } from './pages/users/components/input-bar.component';
import { InputIconComponent } from './pages/users/components/input-icon.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HelpComponent } from './pages/help/help.component';
import { UsersService } from './services/users.service';
import { UsersComponent } from './users.component';
import { UsersCrudComponent } from './pages/users/users-crud.component';


@NgModule({
  declarations: [
    UsersComponent,
    UsersCrudComponent,
    InputBarComponent,
    InputIconComponent,
    HelpComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      {
        path: '', component: UsersComponent,
        children: [
          { path: 'help', component: HelpComponent},
          { path: 'list', component: UsersCrudComponent},
          { path: '**', redirectTo: 'list'},
        ]
      },
    ])
  ],
  providers: [
    UsersService
  ]
})
export class UsersModule { }
