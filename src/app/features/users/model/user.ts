export interface User {
  id: number;
  name: string;
  age: number;
  fontSize: number;
  bitcoins: number;
  gender: string;
  birthday?: number;
  city?: string;
}
