import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Uikit1Component } from './uikit1.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    Uikit1Component,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      { path: '', component: Uikit1Component }
    ])
  ]
})
export class Uikit1Module { }
