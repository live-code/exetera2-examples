import { Component } from '@angular/core';

interface Country {
  id: number;
  label: string;
  cities: City[];
}

interface City {
  id: number;
  name: string;
  description: string;
}


@Component({
  selector: 'app-uikit1',
  template: `
    <div class="container">

     <!-- 
     <app-tabbar>
        <tab title="tab 1"><input type="text"></tab>
        <tab>2</tab>
        <tab>3</tab>
      </app-tabbar>
      -->

      
      <div style="background-color: white">
        <app-tabbar 
          [data]="countries" 
          [active]="activeCountry"
          icon="fa fa-trash"
          (tabClick)="setActiveCountry($event)"
          (iconClick)="removeCountry($event)"
        ></app-tabbar>
        
        <app-tabbar 
          [data]="activeCountry?.cities" 
          [active]="activeCity"
          labelField="name"
          icon="fa fa-link"
          (tabClick)="setActiveCity($event)"
          (iconClick)="openWiki($event)"
        ></app-tabbar>
      </div>
      
      <app-static-map [city]="activeCity?.name"></app-static-map>

      <app-card 
        title="Widget 1"
        icon="fa fa-microchip"
        headerClass="bg-dark text-white"
        [marginBottom]="5"
        (iconClick)="openUrl('http://www.google.com')"
      >
        <div class="header">
          <app-hello name="yourname" color="orange"></app-hello>
        </div>
        <div class="footer">
          copyright ciccio balilla
        </div>
      </app-card>

      <app-card 
        title="Click icon to toggle next card" 
        [icon]="visible ? 'fa fa-arrow-down' : 'fa fa-arrow-up'" 
        (iconClick)="visible = !visible"
      >
        <app-static-map class="header" zoom="7" city="Trieste"></app-static-map>
        <div class="header">Toggle visibility</div>
      </app-card>
      
      <app-card title="Widget Map" *ngIf="visible">bla bla</app-card>
      
    </div>
  `,
  styles: [
  ]
})
export class Uikit1Component {
  visible = false;
  activeCountry: Country;
  activeCity: City;

  countries: Country[];

  constructor() {
    setTimeout(() => {
      this.countries = [
        { id: 1000, label: 'Italy',
          cities: [
            { id: 1, name: 'Rome', description: 'bla bla1 '},
            { id: 2, name: 'Verona', description: 'bla bla 2'},
            { id: 3, name: 'Vicenza', description: 'bla bla 2'}
          ]
        },
        { id: 1001, label: 'UK',
          cities: [
            { id: 1, name: 'London', description: 'bla bla '}
          ]
        },
        { id: 1002, label: 'Spain',
          cities: [
            { id: 1, name: 'Madrid', description: 'bla bla '},
            { id: 2, name: 'Barcellona', description: 'bla bla '}
          ]
        },
      ];
      this.setActiveCountry(this.countries[0])
    }, 1000);

  }


  setActiveCountry(country: Country, index = 0) {
   this.activeCountry = country;
   this.activeCity = country.cities[index];
  }
  setActiveCity(city: City) {
    this.activeCity = city;
  }

  openUrl(url: string) {
    window.open(url);
  }

  openWiki(city: City) {
    window.open('https://it.wikipedia.org/wiki/' + city.name)
  }

  removeCountry(country: Country) {
    console.log('remove country')
    const index = this.countries.findIndex(c => c.id === country.id);
    this.countries.splice(index, 1);
    if (country.id === this.activeCountry.id) {
      this.activeCountry = this.countries.length ? this.countries[0] : null;
      this.activeCity = this.activeCountry ?  this.activeCountry.cities[0] : null
    }
  }

}
