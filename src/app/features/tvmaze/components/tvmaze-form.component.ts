import { Component, EventEmitter, OnInit, Output } from '@angular/core';

export interface FormType {
  text: string;
}
@Component({
  selector: 'app-tvmaze-form',
  template: `
    <form #f="ngForm" (submit)="search.emit(f.value)">
      <input class="big-input" type="text" ngModel name="text" required minlength="2">
      <button type="submit" [disabled]="f.invalid" hidden>GO</button>
    </form>
  `,
})
export class TvmazeFormComponent {
  @Output() search: EventEmitter<FormType> = new EventEmitter()
}
