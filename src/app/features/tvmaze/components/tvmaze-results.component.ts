import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Series } from '../model/Series';

@Component({
  selector: 'app-tvmaze-results',
  template: `

    <!--RESULT LIST-->
    <div class="grid">
      <div class="grid-item" *ngFor="let series of data">
        <div class="movie" (click)="itemClick.emit(series)">
          <img
            *ngIf="series.show.image"
            [src]="series.show.image.medium"
          >

          <div *ngIf="!series.show.image" class="noImage"></div>
          <div class="movieText">{{series.show.name}}</div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./tvmaze-results.component.css']
})
export class TvmazeResultsComponent {
  @Input() data: Series[];
  @Output() itemClick: EventEmitter<Series> = new EventEmitter<Series>()

}
