import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Show } from '../model/Series';

@Component({
  selector: 'app-tvmaze-modal',
  template: `
    <div *ngIf="show" class="wrapper">
      <span class="closeButton" (click)="close.emit()">×️</span>
      <div class="content">
        <img [src]="show.image.medium" alt="" class="image">
        <h4>{{show.name}}</h4>
        <div *ngFor="let tag of show.genres" class="tag">
          {{tag}}
        </div>

        <div [innerHTML]="show.summary"></div>
        <a class="button" [href]="show.url" target="_blank">Visit WebSite</a>
      </div>

    </div>
  `,
  styleUrls: ['./tvmaze-modal.component.css']
})
export class TvmazeModalComponent {
    @Input() show: Show;
    @Output() close: EventEmitter<void> = new EventEmitter<void>();
}
