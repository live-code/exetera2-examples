import { Component } from '@angular/core';
import { TvmazeService } from './services/tvmaze.service';

@Component({
  selector: 'app-tvmaze',
  template: `
    <app-tvmaze-form (search)="tvmazeService.submit($event)"></app-tvmaze-form>
    
    <app-tvmaze-results 
      [data]="tvmazeService.result" 
      (itemClick)="tvmazeService.itemClickHandler($event)"
    ></app-tvmaze-results>
    
    <app-tvmaze-modal [show]="tvmazeService.showDetails" (close)="tvmazeService.closeDetails()"></app-tvmaze-modal>
  `,
})
export class TvmazeComponent {
  constructor(public tvmazeService: TvmazeService) {
    tvmazeService.init('friends')
  }
}
