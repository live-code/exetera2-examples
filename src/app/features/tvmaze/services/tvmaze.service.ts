import { Injectable } from '@angular/core';
import { Series, Show } from '../model/Series';
import { HttpClient } from '@angular/common/http';
import { FormType } from '../components/tvmaze-form.component';

@Injectable()
export class TvmazeService {
  result: Series[];
  showDetails: Show;

  constructor(private http: HttpClient) {}

  init(text = '') {
    this.submit({  text });
  }

  submit(formdata: FormType) {
    this.http.get<Series[]>('http://api.tvmaze.com/search/shows?q=' + formdata.text)
      .subscribe(res => this.result = res)
  }

  itemClickHandler(series: Series) {
    console.log(series.show.name);
    this.showDetails = series.show;
  }

  closeDetails() {
    this.showDetails = null;
  }
}
