import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TvmazeComponent } from './tvmaze.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TvmazeFormComponent } from './components/tvmaze-form.component';
import { TvmazeResultsComponent } from './components/tvmaze-results.component';
import { TvmazeModalComponent } from './components/tvmaze-modal.component';
import { TvmazeService } from './services/tvmaze.service';

@NgModule({
  declarations: [
    TvmazeComponent,
    TvmazeFormComponent,
    TvmazeResultsComponent,
    TvmazeModalComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      { path: '', component:  TvmazeComponent}
    ])
  ],
  providers: [
    TvmazeService
  ]
})
export class TvmazeModule { }
