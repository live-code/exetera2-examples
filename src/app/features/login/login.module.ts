import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { SigninComponent } from './components/signin.component';
import { LostpassComponent } from './components/lostpass.component';
import { RegistrationComponent } from './components/registration.component';


@NgModule({
  declarations: [LoginComponent, SigninComponent, LostpassComponent, RegistrationComponent],
  imports: [
    CommonModule,
    LoginRoutingModule
  ]
})
export class LoginModule { }
