import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lostpass',
  template: `
    <p>
      lostpass works!
    </p>
    <button routerLink="../signin">back to login</button>
  `,
  styles: [
  ]
})
export class LostpassComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
