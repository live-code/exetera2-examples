import { Component, EventEmitter, HostBinding, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  template: `
            <div class="card-header">
              <h3>Sign In</h3>
              <div class="d-flex justify-content-end social_icon">
                <span><i class="fab fa-facebook-square"></i></span>
                <span><i class="fab fa-google-plus-square"></i></span>
                <span><i class="fab fa-twitter-square"></i></span>
              </div>
            </div>
            <div class="card-body">
              <form>
                <div class="input-group form-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-user"></i></span>
                  </div>
                  <input type="text" class="form-control" placeholder="username">
  
                </div>
                <div class="input-group form-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-key"></i></span>
                  </div>
                  <input type="password" class="form-control" placeholder="password">
                </div>
                <div class="row align-items-center remember">
                  <input type="checkbox">Remember Me
                </div>
                <div class="form-group">
                  <input type="button" value="Login" class="btn float-right login_btn" (click)="login()">
                </div>
              </form>
            </div>
            <div class="card-footer">
              <div class="d-flex justify-content-center links">
                Don't have an account?
                <a 
                  style="text-decoration: underline; cursor: pointer"
                  [routerLink]="'../registration'"
                >Sign Up</a>
              </div>
              <div class="d-flex justify-content-center">
                <a [routerLink]="'../lostpass'">Forgot your password?</a>
              </div>
            </div>
  `,
  styleUrls: ['./signin.component.css']
})
export class SigninComponent {
  @Output() gotoRegistration: EventEmitter<void> = new EventEmitter<void>()

  constructor(private router: Router) {
  }

  login() {
    // API ....
    this.router.navigateByUrl('home')
  }
}
