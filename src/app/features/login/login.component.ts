import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  template: `
    <div class="bg">
      <button routerLink="./signin">signin</button>
      <button routerLink="./registration">registration</button>
      <button routerLink="./lostpass">lostpass</button>
      <input type="text">
      <hr>
      <div class="container">
        <div class="d-flex justify-content-center h-100">
          <div class="card">
            <router-outlet></router-outlet>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [`
    .bg{
      background-image: url('http://getwallpapers.com/wallpaper/full/a/5/d/544750.jpg') !important;
      background-size: cover;
      background-repeat: no-repeat;
      height: calc(100vh - 80px);
      font-family: 'Numans', sans-serif;
    }

    .card{
      height: 370px;
      margin-top: auto;
      margin-bottom: auto;
      width: 400px;
      background-color: rgba(0,0,0,0.5) !important;
    }
    .container{
      height: 100%;
      align-content: center;
    }
  `]
})
export class LoginComponent implements OnInit {
  sezione = 'signin';

  constructor() { }

  ngOnInit(): void {
  }

}
